import openpyxl
def sma(data):
    b = len(data)
    sma = []
    sum = 0
    count = 0
    count2 = 4
    for i in range(4, b):
        for j in range(count, count2):
            sum += data[j]
        count += 1
        count2 += 1
        mid = sum / 4
        sma.append(mid)
    print('SMA равно = ', sma)
    return sma
def ema(data):
    b = len(data)
    ema = []
    ema1= 1
    F = 2/(1+b)
    for i in range(b):
        ema1=data[i]+(1-F)*ema1
        ema.append(ema1)
    print('EMA равно = ', ema)
    return ema
def medfil(data):
    b = len(data)
    medfil=[]
    for i in range(b-1):
        medfil.append(data[i+1])
    print('Медианный фильтр =', medfil)
    return medfil
wb = openpyxl.load_workbook('data.xlsx')
sheet = wb.active
data = []
for cell in sheet['I']:
    data.append(cell.value)
del data[0]
data.sort()
sma(data)
data1=(sma(data))
ema(data)
data2=(sma(data))
medfil(data)
data3 = (medfil(data))
with open('filter.txt', 'w') as f:
    print((data1), file=f)
    print((data2),file=f)
    print((data3),file=f)
f.close()
