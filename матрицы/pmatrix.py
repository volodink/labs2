import random
import itertools
from multiprocessing import Pool
from timeit import default_timer
start_time = default_timer()
def pmatrix(r1,c1,r2,c2):
#r1 = int(1000)
#c1 = int(1000)
#r2 = int(1000)
#c2 = int(1000)
    A = []
    for i in range(r1):
        A.append([])
        for j in range(c1):
            A[i].append(random.uniform(0,10))
    # print('A = ')
    # for q in A:
    #     print(q)
    # print('')
    B = []
    for q in range(r2):
        B.append([])
        for w in range(c2):
            B[q].append(random.uniform(0,10))
    # #print('B = ')
    # for q in B:
    #     print(q)
    # print('')

    s = 0
    t = []
    C = []
    if len(B) != len(A[0]):
        print("Матрицы не могут быть перемножены")
    else:
        r1 = len(A)
        c1 = len(A[0])
        r2 = c1
        c2 = len(B[0])
        for z in range(0, r1):
            for j in range(0, c2):
                for i in range(0, c1):
                    s = s + A[z][i] * B[i][j]
                t.append(s)
                s = 0
            C.append(t)
            t = []
    return C
r1 = int(1000)
c1 = int(1000)
r2 = int(1000)
c2 = int(1000)
list = []
if __name__ == '__main__':
    pool = Pool(processes=2)
    p1 = pool.apply_async(pmatrix(r1//2,c1//2,r2//2,c2//2))
    p2 = pool.apply_async(pmatrix(r1//2,c1//2,r2//2,c2//2))
    # p3 = pool.apply_async(pmatrix(r1//4,c1//4,r2//4,c2//4))
    # p4 = pool.apply_async(pmatrix(r1//4,c1//4,r2//4,c2//4))
    pool.close()
    pool.join()
    stop_time = default_timer()
stop_time = default_timer()
print(stop_time-start_time)
