
from collections import Counter
import time
from multiprocessing import Pool
from math import log
star_time = time.time()
def uniq_count(message):
    return list(Counter(message).values())

def ent(message):
    return -sum([e/len(message)*log(2,(e/len(message))) for e in uniq_count(message)])
if __name__ == '__main__':
    file = open('sample.txt', 'rb')
    data = file.read(1024 * 1024)
    def counter(data):
        qwe = 0
        data = file.read(1024 * 1024)
        while data:
            qwe = qwe + ent(data)
            #print(qwe)
            data = file.read(1024 * 1024)
    pool = Pool(processes=5)
    pool.map_async(counter,data)
    pool.close()
    pool.join()
    file.close()
    print(time.time()-star_time)
